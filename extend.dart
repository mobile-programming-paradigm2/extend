class Rov {
  Rov({this.name, this.sex});
  var name;
  var sex;

  @override
  String toString() => 'name: $name,sex: $sex';
  String get getMe => "Hello, I'm $name. I'm $sex";

  void getName() => print("Hello, I'm $name");
}

class Character extends Rov {
  Character({var name, var sex, this.position}) : super(name: name, sex: sex);
  var position;

  @override
  String toString() => '${super.toString()}, position: $position';
}

void main() {
  final character =
      Character(name: 'Airi', sex: 'Female', position: 'Assassin');
  character.getName();
  print(character.toString());

  print('-----------------------------------------------');

  final character1 = Character(name: 'Aleister', sex: 'Male', position: 'Mage');
  character1.getName();
  print(character1.toString());

  print('-----------------------------------------------');

  final character2 = Character(name: 'Elsu', sex: 'Male', position: 'Carry');
  character2.getName();
  print(character2.toString());

  print('-----------------------------------------------');

  final character3 = Character(name: 'Grakk', sex: 'Male', position: 'Tank');
  character3.getName();
  print(character3.toString());

  print('-----------------------------------------------');

  final character4 =
      Character(name: 'Florentino', sex: 'Male', position: 'Fighter');
  character4.getName();
  print(character4.toString());

  print('-----------------------------------------------');

  final character5 =
      Character(name: 'Sephera', sex: 'Female', position: 'Support');
  character5.getName();
  print(character5.toString());

  print('-----------------------------------------------');
}
